package clock.socoolby.com.clock.widget.animatorview.animator.clockanimator;

import android.graphics.Canvas;

public class CircleTwoClock extends AbstractClock {

    /**
     * 绘制时钟的圆形和刻度
     */
    protected void drawBorder(Canvas canvas) {
        canvas.save();
        canvas.translate(mCenterX, mCenterY);
        mDefaultPaint.setStrokeWidth(mDefaultScaleWidth);
        mDefaultPaint.setColor(mClockColor);

        canvas.drawCircle(0, 0, mRadius, mDefaultPaint);

        for (int i = 0; i < 60; i++) {
            if (i % 5 == 0) { // 特殊时刻
                mDefaultPaint.setStrokeWidth(mParticularlyScaleWidth);
                mDefaultPaint.setColor(mColorParticularyScale);
                mDefaultPaint.setAlpha(255);
                canvas.drawLine(0, -mRadius+mDefaultScaleWidth*1.2f, 0, -mRadius + mDefaultScaleLength+mDefaultScaleWidth*1.3f, mDefaultPaint);
            } else {          // 一般时刻
                mDefaultPaint.setStrokeWidth(mDefaultScaleWidth);
                mDefaultPaint.setColor(mColorDefaultScale);
                mDefaultPaint.setAlpha(100);
                canvas.drawLine(0, -mRadius+mDefaultScaleWidth, 0, -mRadius + mDefaultScaleLength, mDefaultPaint);
            }
            canvas.rotate(6);
        }

        mDefaultPaint.setStrokeWidth(mDefaultScaleWidth/2);
        mDefaultPaint.setAlpha(100);
        canvas.drawCircle(0, 0, mRadius-mDefaultScaleLength*1.5f, mDefaultPaint);

        canvas.restore();
    }

    @Override
    public String typeName() {
        return TYPE_CIRCLE_TWO;
    }

    public static final String TYPE_CIRCLE_TWO="circleTWO";
}
