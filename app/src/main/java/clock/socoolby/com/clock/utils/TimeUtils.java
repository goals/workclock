package clock.socoolby.com.clock.utils;

import java.util.Calendar;
import java.util.Date;

public class TimeUtils {

    public static boolean isToday(Date toCheckday){
        if(toCheckday==null)
            return false;
        Calendar today=Calendar.getInstance();
        int year=today.get(Calendar.YEAR);
        int moth=today.get(Calendar.MONTH);
        int day=today.get(Calendar.DAY_OF_MONTH);
        today.setTime(toCheckday);
        return year==today.get(Calendar.YEAR)&&moth==today.get(Calendar.MONTH)&&day==today.get(Calendar.DAY_OF_MONTH);
    }

    public static boolean isSameDay(Date base,Date toCheckday){

        return base.getYear()==toCheckday.getYear()&&base.getMonth()==toCheckday.getMonth()&&base.getDate()==toCheckday.getDate();
    }

    public static  boolean isSameTime(Date base,Date toCheckday){
        return base.getHours()==toCheckday.getHours()&&base.getMinutes()==toCheckday.getMinutes()&&base.getSeconds()==toCheckday.getSeconds();
    }

    public static boolean isTimeBefore(Date base,Date toCheckday){
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(base);
        Calendar calendar2=Calendar.getInstance();
        calendar2.setTime(toCheckday);
        return calendar.before(calendar2);
    }
}
