package clock.socoolby.com.clock.fragment.houranimator;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;

import clock.socoolby.com.clock.ClockApplication;
import clock.socoolby.com.clock.R;
import clock.socoolby.com.clock.event.ClockEvent;
import clock.socoolby.com.clock.event.EventManger;
import clock.socoolby.com.clock.fragment.AbstractVideoFragment;
import clock.socoolby.com.clock.viewmodel.GlobalViewModel;
import clock.socoolby.com.clock.viewmodel.ViewModelFactory;

public class HourVideoFragment extends AbstractVideoFragment {

    public static final String NAME="video";

    protected  int hour;

    protected GlobalViewModel globalViewModel;

    public HourVideoFragment(){
        super();
        this.hour=0;
    }

    public HourVideoFragment(int hour) {
        super();
        this.hour=hour;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        globalViewModel= ViewModelProviders.of(getActivity(), new ViewModelFactory(ClockApplication.getInstance().getModel())).get(GlobalViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_hour_video,container,false);
        initMediaPlay(view.findViewById(R.id.tv_hour_video));
        loadForModel();
        return view;
    }

    protected void loadForModel(){
        videoFileName= globalViewModel.getTimeHourVideoPath();
        if(videoFileName==null||videoFileName.isEmpty()){
            this.videoFileName="test.3gp";
            this.fileLocal=1;
        }
    }

    @Override
    protected void onPlayCompletion() {
        //globalViewModel.setTimeHourAnimatorStarting(false);
        //getActivity().onBackPressed();
        EventManger.post(ClockEvent.buildHourAnimator(false));
    }
}
