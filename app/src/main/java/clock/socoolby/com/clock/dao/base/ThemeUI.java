package clock.socoolby.com.clock.dao.base;

import java.sql.Clob;

import e.odbo.data.bean.AutoGeneratorStringKeyBean;

public class ThemeUI extends AutoGeneratorStringKeyBean{

//private static final long serialVersionUID =-4172029310027187458L;

    public final static String ID="ID";
    public final static String NAME="NAME";//样式名称
    public final static String CLOCK_TYPE="CLOCK_TYPE";//数字0，模似1
    public final static String CONFIG_TEXT="CONFIG_TEXT";//jsons格式设置

    protected  String id;
    protected  String name;
    protected  Integer clockType;
    protected String configText;

    public ThemeUI() {
        super();
    }

    @Override
    public String getKey() {
        return id;
    }

    @Override
    public void setKey(String key) {
       this.id=key;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getClockType() {
        return clockType;
    }

    public void setClockType(Integer clockType) {
        this.clockType = clockType;
    }

    public String getConfigText() {
        return configText;
    }

    public void setConfigText(String configText) {
        this.configText = configText;
    }

    public String toString() {
        return "id="+id
                +",name="+name
                +",clockType="+clockType
                +",configText="+configText;
    }
}