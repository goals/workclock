package clock.socoolby.com.clock.net.protocol;

import org.json.JSONException;
import org.json.JSONObject;

import clock.socoolby.com.clock.net.base.I_JsonObject;

public class ResponseBase implements I_JsonObject {

    protected int mResultCode = ProtocolConstants.RESULT_FAILED;

    public int getResultCode() {
        return mResultCode;
    }

    public boolean parseResponse(JSONObject object) {
        if (object == null) {
            return false;
        }
        try {
            mResultCode = object.optInt(ProtocolConstants.STR_RESULT_CODE, 0);
            if (mResultCode == ProtocolConstants.RESULT_OK) {
                return parse(object);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean parse(JSONObject object) throws JSONException {
        return true;
    }

    @Override
    public void fromJson(JSONObject jsonObject) throws JSONException {
        parseResponse(jsonObject);
    }

    @Override
    public void toJson(JSONObject jsonObject) throws JSONException {

    }
}
