package clock.socoolby.com.clock.net.base;

import org.json.JSONException;
import org.json.JSONObject;

public interface I_JsonObject {

     void fromJson(JSONObject jsonObject) throws JSONException ;


     void toJson(JSONObject jsonObject) throws JSONException;
}
