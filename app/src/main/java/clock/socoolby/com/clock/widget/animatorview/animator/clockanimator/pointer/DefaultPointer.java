package clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.pointer;

public class DefaultPointer extends AbstractPointer {
    public static final String TYPE_NAME="default";


    @Override
    public String typeName() {
        return TYPE_NAME;
    }
}
