package clock.socoolby.com.clock.todo.microsoft.bean.todo;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.Date;

import clock.socoolby.com.clock.todo.microsoft.bean.AbstractChangeLogMircsoftEntity;
import clock.socoolby.com.clock.todo.microsoft.bean.base.DateTimeTimeZone;
import clock.socoolby.com.clock.todo.microsoft.bean.base.ItemBody;
import clock.socoolby.com.clock.todo.microsoft.bean.base.PatternedRecurrence;
import clock.socoolby.com.clock.todo.microsoft.utils.TypeUtils;
import clock.socoolby.com.clock.utils.JsonUtils;

/**
 * 属性	类型	说明
 * assignedTo	        String	Outlook 中已为其分配任务的人员姓名。 只读。
 * body	                itemBody	通常包含有关任务的信息的任务正文。 请注意，仅支持 HTML 类型。
 * categories	        String 集合	与任务关联的类别。 每个类别对应于用户定义的 outlookCategory 的 displayName 属性。
 * changeKey	        String	任务的版本。
 * completedDateTime	dateTimeTimeZone	在指定时区内完成任务的日期。
 * createdDateTime	    DateTimeOffset	任务的创建日期和时间。 默认情况下，它采用 UTC 格式。 你可以在请求标头中提供自定义时区。 属性值使用 ISO 8601 格式。 例如，2014 年 1 月 1 日午夜 UTC 如下所示：'2014-01-01T00:00:00Z'。
 * dueDateTime	        dateTimeTimeZone	要在指定时区内完成任务的日期。
 * hasAttachments	    Boolean	如果任务包含附件，则设置为 true。
 * id	                String	任务的唯一标识符。 默认情况下, 在将项目从一个容器 (如文件夹或日历) 移动到另一个容器时, 此值会发生更改。 若要更改此行为, 请Prefer: IdType="ImmutableId"使用标头。 有关详细信息, 请参阅获取 Outlook 资源的不可变标识符。 只读。
 * importance	        string	事件的重要性。 可取值为：low、normal、high。
 * isReminderOn	        Boolean	如果设置警报以提醒用户有任务，则设置为 true。
 * lastModifiedDateTime	DateTimeOffset	上次修改任务的日期和时间。 默认情况下，它采用 UTC 格式。 你可以在请求标头中提供自定义时区。 属性值使用 ISO 8601 格式，并始终处于 UTC 时间。 例如，2014 年 1 月 1 日午夜 UTC 如下所示：'2014-01-01T00:00:00Z'。
 * owner	            String	任务创建者的姓名。
 * parentFolderId	    String	任务的父文件夹的唯一标识符。
 * recurrence	        patternedRecurrence	任务的定期模式。
 * reminderDateTime	    dateTimeTimeZone	提醒警报发出任务发生提醒的日期和时间。
 * sensitivity	        string	指示任务的隐私级别。 可取值为：normal、personal、private、confidential。
 * startDateTime	    dateTimeTimeZone	要在指定时区内开始执行任务的日期。
 * status	            string	指示任务的状态或进度。 可取值为：notStarted、inProgress、completed、waitingOnOthers、deferred。
 * subject	            String	任务的简要说明或标题。
 *
 */


public class TodoEntity extends AbstractChangeLogMircsoftEntity {
    public static final String ASSIGNEDTO="assignedTo";
    private String assignedto;

    private ItemBody body;
    private String[] categories;

    public static final String COMPLETEDDATETIME="completedDateTime";
    private DateTimeTimeZone completeddatetime;

    public static final String CREATEDDATETIME="createdDateTime";
    private Date createddatetime;
    public static final String DUEDATETIME="dueDateTime";
    private DateTimeTimeZone duedatetime;
    public static final String HASATTACHMENTS= "hasAttachments";
    private Boolean hasattachments;

    public static final String IMPORTANCE= "importance";
    private TodoImportanceEnum importance;

    public static final String ISREMINDERON="isReminderOn";
    private Boolean isreminderon;

    public static final String LASTMODIFIEDDATETIME="lastModifiedDateTime";
    private Date lastmodifieddatetime;

    private String owner;

    public static final String PARENTFOLDERID="parentFolderId";
    private String parentfolderid;

    private PatternedRecurrence recurrence;

    public static final String REMINDERDATETIME="reminderDateTime";
    private DateTimeTimeZone reminderdatetime;

    private TodoSensitivityEnum sensitivity;

    public static final String STARTDATETIME="startDateTime";
    private DateTimeTimeZone startdatetime;

    private TodoStatusEnum status;

    private String subject;

    @Override
    public void fromJson(JSONObject jsonObject) throws JSONException {
        super.fromJson(jsonObject);
        assignedto=jsonObject.getString(ASSIGNEDTO);
        if(!jsonObject.isNull("body")) {
            body=new ItemBody();
            body.fromJson(jsonObject.getJSONObject("body"));
        }
        categories= JsonUtils.readStringArray(jsonObject,"categories");

        try {
            if(!jsonObject.isNull(COMPLETEDDATETIME)) {
                completeddatetime = new DateTimeTimeZone();
                completeddatetime.fromJson(jsonObject.getJSONObject(COMPLETEDDATETIME));
            }


            if(!jsonObject.isNull(DUEDATETIME)) {
                duedatetime = new DateTimeTimeZone();
                duedatetime.fromJson(jsonObject.getJSONObject(DUEDATETIME));
            }

            if(!jsonObject.isNull(STARTDATETIME)) {
                startdatetime = new DateTimeTimeZone();
                startdatetime.fromJson(jsonObject.getJSONObject(STARTDATETIME));
            }

            if(!jsonObject.isNull(REMINDERDATETIME)) {
                reminderdatetime = new DateTimeTimeZone();
                reminderdatetime.fromJson(jsonObject.getJSONObject(REMINDERDATETIME));
            }

            createddatetime= TypeUtils.deserialize(jsonObject.getString(CREATEDDATETIME));
            lastmodifieddatetime=TypeUtils.deserialize(jsonObject.getString(LASTMODIFIEDDATETIME));

        } catch (ParseException e) {
            e.printStackTrace();
        }
        hasattachments=jsonObject.getBoolean(HASATTACHMENTS);

        importance=TodoImportanceEnum.build(jsonObject.getString(IMPORTANCE));
        isreminderon=jsonObject.getBoolean(ISREMINDERON);
        owner=jsonObject.getString("owner");
        parentfolderid=jsonObject.getString(PARENTFOLDERID);


        if(!jsonObject.isNull("recurrence")) {
            recurrence=new PatternedRecurrence();
            recurrence.fromJson(jsonObject.getJSONObject("recurrence"));
        }

        sensitivity=TodoSensitivityEnum.build(jsonObject.getString("sensitivity"));

        status=TodoStatusEnum.valueOf(jsonObject.getString("status"));

        subject=jsonObject.getString("subject");
    }


    @Override
    public void toJson(JSONObject jsonObject) throws JSONException {

    }

    public String getAssignedto() {
        return assignedto;
    }

    public void setAssignedto(String assignedto) {
        this.assignedto = assignedto;
    }

    public ItemBody getBody() {
        return body;
    }

    public void setBody(ItemBody body) {
        this.body = body;
    }

    public String[] getCategories() {
        return categories;
    }

    public void setCategories(String[] categories) {
        this.categories = categories;
    }


    public Date getCreateddatetime() {
        return createddatetime;
    }

    public void setCreateddatetime(Date createddatetime) {
        this.createddatetime = createddatetime;
    }


    public Boolean getHasattachments() {
        return hasattachments;
    }

    public void setHasattachments(Boolean hasattachments) {
        this.hasattachments = hasattachments;
    }

    public TodoImportanceEnum getImportance() {
        return importance;
    }

    public void setImportance(TodoImportanceEnum importance) {
        this.importance = importance;
    }

    public Boolean getIsreminderon() {
        return isreminderon;
    }

    public void setIsreminderon(Boolean isreminderon) {
        this.isreminderon = isreminderon;
    }

    public Date getLastmodifieddatetime() {
        return lastmodifieddatetime;
    }

    public void setLastmodifieddatetime(Date lastmodifieddatetime) {
        this.lastmodifieddatetime = lastmodifieddatetime;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getParentfolderid() {
        return parentfolderid;
    }

    public void setParentfolderid(String parentfolderid) {
        this.parentfolderid = parentfolderid;
    }

    public PatternedRecurrence getRecurrence() {
        return recurrence;
    }

    public void setRecurrence(PatternedRecurrence recurrence) {
        this.recurrence = recurrence;
    }


    public void setStatus(TodoStatusEnum status) {
        this.status = status;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public TodoSensitivityEnum getSensitivity() {
        return sensitivity;
    }

    public void setSensitivity(TodoSensitivityEnum sensitivity) {
        this.sensitivity = sensitivity;
    }

    public DateTimeTimeZone getCompleteddatetime() {
        return completeddatetime;
    }

    public void setCompleteddatetime(DateTimeTimeZone completeddatetime) {
        this.completeddatetime = completeddatetime;
    }

    public DateTimeTimeZone getDuedatetime() {
        return duedatetime;
    }

    public void setDuedatetime(DateTimeTimeZone duedatetime) {
        this.duedatetime = duedatetime;
    }

    public DateTimeTimeZone getReminderdatetime() {
        return reminderdatetime;
    }

    public void setReminderdatetime(DateTimeTimeZone reminderdatetime) {
        this.reminderdatetime = reminderdatetime;
    }

    public DateTimeTimeZone getStartdatetime() {
        return startdatetime;
    }

    public void setStartdatetime(DateTimeTimeZone startdatetime) {
        this.startdatetime = startdatetime;
    }

    public TodoStatusEnum getStatus() {
        return status;
    }
}
