package clock.socoolby.com.clock.todo.microsoft.bean.base;


/**
 *
 * 每天	daily	事件按 interval 指定的时间间隔天数重复发生。	事件每 3 天重复发生一次。	type、interval
 * 每周	weekly	事件按时间间隔周数在一周内的一天或几天重复发生。	事件每两个星期一和星期二重复发生一次。	type、interval、daysOfWeek、firstDayOfWeek
 * 绝对每月	absoluteMonthly	事件按时间间隔月数在相应月份的指定一天（例如 15 号）重复发生。	事件每季度（每 3 个月）的 15 号重复发生一次。	type、interval、dayOfMonth
 * 相对每月	relativeMonthly	事件按时间间隔月数在一周内的指定一天或几天（相应月份的同一相对位置）重复发生。	事件每 3 个月的第二个星期四或星期五重复发生一次。	type、interval、daysOfWeek
 * 绝对每年	absoluteYearly	事件按时间间隔年数在指定月份的一天重复发生。	事件每 3 年在 3 月 15 日重复发生一次。	type、interval、dayOfMonth、month
 * 相对每年	relativeYearly	事件按时间间隔年数在一周内的指定一天或几天（相应年份和月份的同一相对位置）重复发生。	事件每 3 年在 11 月的第二个星期四或星期五重复发生一次。	type、interval、daysOfWeek、month
 */
public enum RecurrencePatternTypeEnum {
    daily,weekly,absoluteMonthly,relativeMonthly,absoluteYearly,relativeYearly
}
