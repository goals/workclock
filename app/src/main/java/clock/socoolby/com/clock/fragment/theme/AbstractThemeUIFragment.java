package clock.socoolby.com.clock.fragment.theme;

import android.annotation.TargetApi;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.util.Timer;
import java.util.TimerTask;

import clock.socoolby.com.clock.ClockApplication;
import clock.socoolby.com.clock.FamousQuotesManager;
import clock.socoolby.com.clock.MainActivity;
import clock.socoolby.com.clock.R;
import clock.socoolby.com.clock.model.DateModel;
import clock.socoolby.com.clock.model.SharePerferenceModel;
import clock.socoolby.com.clock.pop.CalendarPopup;
import clock.socoolby.com.clock.pop.ColorPickerPop;
import clock.socoolby.com.clock.pop.TimeSetupMinPopup;
import clock.socoolby.com.clock.pop.TimeSetupPopup;
import clock.socoolby.com.clock.pop.WeatherPopup;
import clock.socoolby.com.clock.state.ClockInterfaceTypeEnum;
import clock.socoolby.com.clock.state.ClockThemeUITypeEnum;
import clock.socoolby.com.clock.state.ClockTimeTypeEnum;
import clock.socoolby.com.clock.utils.DialogUtils;
import clock.socoolby.com.clock.viewmodel.AlterViewModel;
import clock.socoolby.com.clock.viewmodel.DigitViewModel;
import clock.socoolby.com.clock.viewmodel.GlobalViewModel;
import clock.socoolby.com.clock.viewmodel.SimulateViewModel;
import clock.socoolby.com.clock.viewmodel.ThemeUIViewModel;
import clock.socoolby.com.clock.viewmodel.ViewModelFactory;
import clock.socoolby.com.clock.widget.textview.AutoScrollTextView;
import timber.log.Timber;

public abstract class AbstractThemeUIFragment extends Fragment implements View.OnClickListener {

    public static final String TAG= AbstractThemeUIFragment.class.getSimpleName();

    public int layoutId;

    private TextView tv_date;
    private TextView tv_day;
    private TextView tv_weather;
    private AutoScrollTextView tv_descript;
    private ImageButton tv_setting;
    private ImageButton tv_handup;

    private ImageButton tv_hours_system;
    private ImageButton tv_background_image_hand;
    private ImageButton tv_break;

    private ImageButton tv_clock_interface;

    //颜色相关
    private ImageButton tv_background_color;
    private ImageButton tv_foreground_color1;
    private ImageButton tv_foreground_color2;

    private TextView tv_hand_time;

    private ImageButton tv_screen_lock;

    ColorPickerPop colorPickerDialog;
    TimeSetupMinPopup handUpTimePopup;
    CalendarPopup calendarPopup;
    WeatherPopup weatherPopup;

    GlobalViewModel globalViewModel;
    DigitViewModel digitViewModel;
    SimulateViewModel simulateViewModel;
    ThemeUIViewModel themeUIViewModel;
    AlterViewModel  alterViewModel;

    MainActivity mainActivity;

    private SharePerferenceModel model;

    private boolean autoFullScreen=false;

    private int runDelaySecond=10000;

    private boolean running=false;

    private Timer timer=null;

    public AbstractThemeUIFragment(int layoutId){
        this.layoutId=layoutId;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.globalViewModel = ViewModelProviders.of(getActivity(), new ViewModelFactory(ClockApplication.getInstance().getModel())).get(GlobalViewModel.class);
        this.digitViewModel = ViewModelProviders.of(getActivity(), new ViewModelFactory(ClockApplication.getInstance().getModel())).get(DigitViewModel.class);
        this.simulateViewModel = ViewModelProviders.of(getActivity(), new ViewModelFactory(ClockApplication.getInstance().getModel())).get(SimulateViewModel.class);
        this.themeUIViewModel = ViewModelProviders.of(getActivity(), new ViewModelFactory(ClockApplication.getInstance().getModel())).get(ThemeUIViewModel.class);
        this.alterViewModel=ViewModelProviders.of(getActivity(), new ViewModelFactory(ClockApplication.getInstance().getModel())).get(AlterViewModel.class);
        this.mainActivity = (MainActivity)getActivity();
        this.model = ClockApplication.getInstance().getModel();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View themeRootView=inflater.inflate(layoutId, container, false);

        tv_background_image_hand = themeRootView.findViewById(R.id.tv_background_image_hand);
        tv_background_image_hand.setOnClickListener(this);
        tv_background_image_hand.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if(model.isUseSystemWallpaper()){
                    DialogUtils.show(getActivity(), getString(R.string.system_wallpaper), getString(R.string.logut_system_wallpaper_mode), new DialogUtils.OkCancelSelectedLinstener() {
                        @Override
                        public void onReturn(boolean ok) {
                            if(ok) {
                                globalViewModel.setUseSystemWallpaper(false);
                                mainActivity.configBackGroundImage();
                            }
                        }
                    });
                }else
                  mainActivity.configBackGroundImage();
                return true;
            }
        });

        tv_date = themeRootView.findViewById(R.id.tv_date);
        tv_date.setOnClickListener(this);
        tv_date.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                changeThemeTypeCheck();
                return true;
            }
        });

        tv_day = themeRootView.findViewById(R.id.tv_day);
        tv_day.setOnClickListener(this);
        tv_day.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (isScreenLock())
                    return true;
                globalViewModel.setHourSystem12(!model.isHourSystem12());
                return true;
            }
        });

        tv_weather = themeRootView.findViewById(R.id.tv_weather);
        tv_weather.setOnClickListener(this);
        tv_descript = themeRootView.findViewById(R.id.tv_descript);
        tv_descript.setOnClickListener(this);
        tv_descript.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //changeBackGroundAnimator(ANIMATOR_TEXTLED);
                return true;
            }
        });

        tv_break = themeRootView.findViewById(R.id.tv_break);
        tv_break.setOnClickListener(this);
        tv_break.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mainActivity.getClockStateMachine().handUpCountingDownCheck();
                return true;
            }
        });

        tv_handup = themeRootView.findViewById(R.id.tv_hand);
        tv_handup.setOnClickListener(this);
        tv_handup.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (isScreenLock())
                    return true;
                setupHandUpTime();
                return true;
            }
        });
        tv_hand_time = themeRootView.findViewById(R.id.tv_hand_time);
        tv_hand_time.setOnClickListener(this);
        tv_hand_time.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                tv_hand_time.setVisibility(View.GONE);
                return true;
            }
        });

        tv_hours_system = themeRootView.findViewById(R.id.tv_hours_system);

        tv_background_color = themeRootView.findViewById(R.id.tv_background_color);
        tv_background_color.setOnClickListener(this);

        tv_foreground_color1 = themeRootView.findViewById(R.id.tv_foreground_color);
        tv_foreground_color1.setOnClickListener(this);
        tv_foreground_color1.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (isScreenLock())
                    return true;
                if (colorPickerDialog == null)
                    colorPickerDialog = new ColorPickerPop(getActivity());

                colorPickerDialog.setOnColorChangeListenter(new ColorPickerPop.OnColorListener() {
                    @Override
                    public void onEnsure(int color) {
                        model.setForegroundColor1(color);
                        globalViewModel.setForegroundColor(color);
                        tv_foreground_color1.setColorFilter(color);
                        autoFullScreenCheck();
                    }

                    @Override
                    public void onBack() {
                    }
                });
                colorPickerDialog.show(model.getForegroundColor());
                return true;
            }
        });

        tv_foreground_color2 = themeRootView.findViewById(R.id.tv_foreground_color1);
        tv_foreground_color2.setOnClickListener(this);
        tv_foreground_color2.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (isScreenLock())
                    return true;
                if (colorPickerDialog == null)
                    colorPickerDialog = new ColorPickerPop(getActivity());

                colorPickerDialog.setOnColorChangeListenter(new ColorPickerPop.OnColorListener() {
                    @Override
                    public void onEnsure(int color) {
                        model.setForegroundColor2(color);
                        globalViewModel.setForegroundColor(color);
                        tv_foreground_color2.setColorFilter(color);
                        autoFullScreenCheck();
                    }

                    @Override
                    public void onBack() {
                    }
                });
                colorPickerDialog.show(model.getForegroundColor1());
                return true;
            }
        });

        tv_clock_interface = themeRootView.findViewById(R.id.tv_clock_interface);
        tv_clock_interface.setOnClickListener(this);


        tv_setting = themeRootView.findViewById(R.id.tv_setting);
        tv_setting.setOnClickListener(this);


        tv_screen_lock = themeRootView.findViewById(R.id.tv_screen_lock);
        tv_screen_lock.setOnClickListener(this);
        tv_screen_lock.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if(isScreenLock())
                    return true;
                setupScreenLockPassword();
                return true;
            }
        });

        bindViewModel();
        return themeRootView;
    }


    private void autoFullScreenCheck(){
        if(!autoFullScreen)
            return;
        if(timer==null)
            timer = new Timer();
        if(running){
            timer.cancel();
        }
        running=true;
        Timber.d("autoFullScreen task created ...");
        timer.schedule(new TimerTask() {
            public void run() {
                themeUIViewModel.setClockUITypeEnum(ClockThemeUITypeEnum.FULLSCREEN);
                this.cancel();
            }
        }, runDelaySecond);
    }


    abstract void changeThemeTypeCheck();

    private void bindViewModel() {

        themeUIViewModel.getClockUITypeEnum().observe(mainActivity, new Observer<ClockThemeUITypeEnum>() {
            @Override
            public void onChanged(ClockThemeUITypeEnum uiTypeEnum) {
                switchMode(uiTypeEnum);
            }
        });

        globalViewModel.getHourSystem12().observe(mainActivity, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                tv_hours_system.setVisibility(aBoolean ? View.VISIBLE : View.GONE);
                if(aBoolean)
                    tv_hours_system.setImageResource(globalViewModel.getTime_hour().getValue() > 12 ? R.drawable.ic_pm : R.drawable.ic_am);
            }
        });

        globalViewModel.getTime_hour().observe(mainActivity, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                tv_hours_system.setImageResource(integer > 12 ? R.drawable.ic_pm : R.drawable.ic_am);
            }
        });

        themeUIViewModel.getDescription().observe(mainActivity, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                setDiscript(s);
            }
        });

        globalViewModel.getForegroundColor().observe(mainActivity, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                setForegroundColor(integer);
                updateColorSettingButtunColor();
            }
        });

        globalViewModel.getHandUpAble().observe(mainActivity, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean handUpAbla) {
                if(handUpAbla) {
                    globalViewModel.getHandUpTime().setValue(model.getHandUpTime());
                    tv_handup.setColorFilter(globalViewModel.getForegroundColor().getValue());
                    tv_hand_time.setVisibility(View.VISIBLE);
                }else{
                    tv_handup.setColorFilter(R.color.colorPrimaryDark);
                    tv_hand_time.setVisibility(View.GONE);
                    if(mainActivity.getClockStateMachine().getCurrentShowTimeType()== ClockTimeTypeEnum.COUNTING_DOWN)
                        mainActivity.getClockStateMachine().setCurrentShowTimeType(ClockTimeTypeEnum.TIME);
                }
            }
        });

        globalViewModel.getHandUpTime().observe(mainActivity, new Observer<Integer>() {
            @Override
            public void onChanged(Integer handUpTime) {
                tv_hand_time.setText(DateModel.getTime(handUpTime));
            }
        });

        themeUIViewModel.getHand_time_visable().observe(mainActivity, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                tv_hand_time.setVisibility(aBoolean ? View.VISIBLE : View.GONE);
            }
        });


        themeUIViewModel.getWeatherDescription().observe(mainActivity, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                tv_weather.setText(s);
            }
        });


        themeUIViewModel.getWeekDescription().observe(mainActivity, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                tv_date.setText(s);
            }
        });

        themeUIViewModel.getDayDescription().observe(mainActivity, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                tv_day.setText(s);
            }
        });

        globalViewModel.getSrceenLock().observe(mainActivity,locked->screenLock(locked));
    }


    private void updateColorSettingButtunColor(){
        tv_foreground_color1.setColorFilter(model.getForegroundColor1());
        tv_foreground_color2.setColorFilter(model.getForegroundColor2());
    }

    private boolean isScreenLock() {
        return globalViewModel.getSrceenLock().getValue();
    }

    private boolean isHandUpAble() {
        return globalViewModel.getHandUpAble().getValue();
    }


    //TimeSetupPopup handUpTimePopup1;

    private void setupHandUpTime() {
        int saveHandUpTime=model.getHandUpTime();
        int hour=0;
        int minute=0;
        TimeSetupPopup handUpTimePopup1 = new TimeSetupPopup();
        handUpTimePopup1.setOnSeekBarChangeListener(new TimeSetupPopup.OnTimeChangeListener() {
                @Override
                public void onChanged(int hour, int minute, Uri uri, String s, String hundUpStyle) {
                    if (hour == 0 && minute == 0) {
                        model.setHandUpTime(-1);
                        globalViewModel.setHandUpAble(false);
                    } else {
                        model.setHandUpTime(hour * 60 + minute);
                        globalViewModel.setHandUpTime(hour * 60 + minute);
                        globalViewModel.setHandUpAble(true);
                        globalViewModel.setHandUpTypeName(hundUpStyle);
                        alterViewModel.setHandUpMusic(uri);
                        alterViewModel.setHandUpMusicName(s);
                    }
                }
            });
        if(saveHandUpTime>0){
            hour=saveHandUpTime/60;
            minute=saveHandUpTime-hour*60;
        }
        timber.log.Timber.d("switch HandUpTime:" + saveHandUpTime+"\thour:"+hour+"\tminute:"+minute);
        handUpTimePopup1.init(hour,minute,globalViewModel.getHandUpTypeName().getValue(),alterViewModel.getHandUpMusic().getValue(),alterViewModel.getHandUpMusicName().getValue());
        handUpTimePopup1.showNow(getFragmentManager(),null);
    }

    private void setupTempHandUpTime() {
        int saveHandUpTime=globalViewModel.getHandUpTime().getValue();
        int hour=0;
        int minute=0;
        if(handUpTimePopup==null) {
            handUpTimePopup = new TimeSetupMinPopup(mainActivity);
        }
        handUpTimePopup.setOnSeekBarChangeListener(new TimeSetupMinPopup.OnTimeChangeListener() {
            @Override
            public void onChanged(int hour, int minute) {
                globalViewModel.getHandUpTime().setValue(hour*60+minute);
            }
        });
        hour=saveHandUpTime/60;
        minute=saveHandUpTime-hour*60;
        handUpTimePopup.init(hour,minute);
        handUpTimePopup.showPopupWindow();
    }

    private void setupScreenLockPassword(){
        int password=globalViewModel.getScreenLockPassword();
        int hour=0;
        int minute=0;
        if(handUpTimePopup==null) {
            handUpTimePopup = new TimeSetupMinPopup(mainActivity);
        }
        handUpTimePopup.setOnSeekBarChangeListener(new TimeSetupMinPopup.OnTimeChangeListener() {
            @Override
            public void onChanged(int hour, int minute) {
                globalViewModel.setScreenLockPassword(hour*60+minute);
                Toast.makeText(getActivity(),"设置成功，请记好你的密码："+hour+":"+minute,Toast.LENGTH_LONG).show();
            }
        });
        hour=password/60;
        minute=password-hour*60;
        handUpTimePopup.init(hour,minute);
        handUpTimePopup.showPopupWindow();
    }

    private void checkScreenLockPassword(){
        int hour=0;
        int minute=0;
        if(handUpTimePopup==null) {
            handUpTimePopup = new TimeSetupMinPopup(mainActivity);
        }
        handUpTimePopup.setOnSeekBarChangeListener(new TimeSetupMinPopup.OnTimeChangeListener() {
            @Override
            public void onChanged(int hour, int minute) {
                int password=globalViewModel.getScreenLockPassword();
                if(password==(hour*60+minute)) {
                    globalViewModel.setSrceenLock(false);
                    Toast.makeText(getActivity(),"密码校验成功，退出锁屏.",Toast.LENGTH_SHORT).show();
                }else
                    Toast.makeText(getActivity(),"密码校验出错，请重试.",Toast.LENGTH_SHORT).show();
            }
        });
        handUpTimePopup.init(hour,minute);
        handUpTimePopup.showPopupWindow();
    }


    //Discript
    private void setDiscript(String disp) {
        if(disp==null||disp.isEmpty())
            disp = FamousQuotesManager.roundFamousQuotes();
        tv_descript.setText(disp);
        tv_descript.startScrollIfAble();
    }

    private void setDiscriptForModel() {
        String dis = model.getDescription();
        if (dis == null || dis.isEmpty())
            dis = FamousQuotesManager.roundFamousQuotes();
        setDiscript(dis);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onClick(View view) {
        if (isScreenLock() && view.getId() != R.id.tv_screen_lock) {
            return;
        }
        switch (view.getId()) {
            case R.id.tv_setting:
                mainActivity.setup();
                break;
            case R.id.tv_hand:
                int handUpTime=model.getHandUpTime();
                boolean handUpAbla=isHandUpAble();
                if(!handUpAbla&&handUpTime<0) {
                    setupHandUpTime();
                }else{
                    handUpAbla=!handUpAbla;
                    globalViewModel.setHandUpAble(handUpAbla);
                    themeUIViewModel.setDescription(FamousQuotesManager.roundAutoQuotes());
                }
                break;
            case R.id.tv_hand_time:
                setupTempHandUpTime();
                break;
            case R.id.tv_day:
                digitViewModel.setDisplaySecond(!model.isDisplaySecond());
                break;
            case R.id.tv_date:
                if (calendarPopup == null)
                    calendarPopup = new CalendarPopup(mainActivity.getAlterManager(),mainActivity.getTodoSyncServiceManager());
                calendarPopup.showNow(getFragmentManager(),"test");
                calendarPopup.setCurrentDay();
                break;
            case R.id.tv_weather:
                if(mainActivity.weatherAdape==null) {
                    Toast.makeText(getActivity(),"天气数据获取失败，请检查网络",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (weatherPopup == null)
                    weatherPopup = new WeatherPopup(mainActivity);
                weatherPopup.init(mainActivity.weatherAdape.getWeatherList(), model.getCity() + "    PM2.5 : " + mainActivity.weatherAdape.getmPM25());
                weatherPopup.showPopupWindow();
                break;
            case R.id.tv_screen_lock:
                if(isScreenLock()&&globalViewModel.getScreenLockPassword()>0){
                    checkScreenLockPassword();
                }else
                   globalViewModel.setSrceenLock(!globalViewModel.getSrceenLock().getValue());
                break;
            case R.id.tv_break:
                mainActivity.getClockStateMachine().countingCheck();
                break;
            case R.id.tv_descript:
                setDiscript(FamousQuotesManager.roundAutoQuotes());
                break;
            case R.id.tv_background_color:
                if(model.isUseSystemWallpaper()){
                    DialogUtils.show(getActivity(), getString(R.string.system_wallpaper), getString(R.string.logut_system_wallpaper_mode), new DialogUtils.OkCancelSelectedLinstener() {
                        @Override
                        public void onReturn(boolean ok) {
                            if(ok) {
                                globalViewModel.setUseSystemWallpaper(false);
                            }
                        }
                    });
                }else {
                    if (colorPickerDialog == null)
                        colorPickerDialog = new ColorPickerPop(mainActivity);

                    colorPickerDialog.setOnColorChangeListenter(new ColorPickerPop.OnColorListener() {
                        @Override
                        public void onEnsure(int color) {
                            globalViewModel.setBackgroundColor(color);
                        }

                        @Override
                        public void onBack() {
                        }
                    });
                    colorPickerDialog.show(model.getBackgroundColor());
                }
                break;
            case R.id.tv_background_image_hand:
                if(model.isUseSystemWallpaper()){
                    DialogUtils.show(getActivity(),  getString(R.string.system_wallpaper), getString(R.string.logut_system_wallpaper_mode), new DialogUtils.OkCancelSelectedLinstener() {
                        @Override
                        public void onReturn(boolean ok) {
                            if(ok) {
                                globalViewModel.setUseSystemWallpaper(false);
                                if (globalViewModel.getBackgroundImageUri().getValue() == null)
                                    mainActivity.configBackGroundImage();
                                else
                                    globalViewModel.setBackgroundImageVisable(true);
                            }
                        }
                    });
                }else {
                    if (globalViewModel.getBackgroundImageUri().getValue() == null)
                        mainActivity.configBackGroundImage();
                    else
                        globalViewModel.setBackgroundImageVisable(!globalViewModel.getBackgroundImageVisable().getValue());
                }
                break;
            case R.id.tv_foreground_color:
                globalViewModel.setForegroundColor(model.getForegroundColor1());
                break;
            case R.id.tv_foreground_color1:
                globalViewModel.setForegroundColor(model.getForegroundColor2());
                break;
            case R.id.tv_clock_interface:
                changeClockInterface();
                break;
        }
        autoFullScreenCheck();
    }


    protected void changeClockInterface() {
        ClockInterfaceTypeEnum currentClockInterface = globalViewModel.getClockInterfaceTypeEnum().getValue();
        switch (currentClockInterface) {
            case Digit:
                currentClockInterface = ClockInterfaceTypeEnum.Simulate;
                break;
            default:
                currentClockInterface = ClockInterfaceTypeEnum.Digit;
        }
        globalViewModel.setClockInterfaceTypeEnum(currentClockInterface);
    }

    protected void setForegroundColor(Integer color) {
        tv_date.setTextColor(color);
        tv_day.setTextColor(color);
        tv_weather.setTextColor(color);
        tv_descript.setTextColor(color);

        tv_handup.setColorFilter(color);
        tv_screen_lock.setColorFilter(color);
        tv_setting.setColorFilter(color);
        tv_background_color.setColorFilter(color);
        tv_hand_time.setTextColor(color);
        tv_hours_system.setColorFilter(color);
        tv_background_image_hand.setColorFilter(color);

        tv_break.setColorFilter(color);

        tv_clock_interface.setColorFilter(color);
    }


    private void switchMode(ClockThemeUITypeEnum mode) {
        timber.log.Timber.d("switch mode to:" + mode);
        switch (mode) {
            case FULLSCREEN:
                tv_date.setVisibility(View.GONE);
                tv_day.setVisibility(View.GONE);
                tv_weather.setVisibility(View.GONE);
                tv_descript.setVisibility(View.GONE);

                tv_handup.setVisibility(View.GONE);

                tv_setting.setVisibility(View.GONE);
                tv_background_color.setVisibility(View.GONE);

                tv_background_color.setVisibility(View.GONE);
                tv_foreground_color1.setVisibility(View.GONE);
                tv_foreground_color2.setVisibility(View.GONE);
                tv_background_image_hand.setVisibility(View.GONE);

                tv_break.setVisibility(View.GONE);

                tv_clock_interface.setVisibility(View.GONE);

                tv_screen_lock.setVisibility(View.GONE);
                break;
            case NORMAL:
                tv_date.setVisibility(View.VISIBLE);
                tv_day.setVisibility(View.VISIBLE);
                tv_weather.setVisibility(View.VISIBLE);
                tv_descript.setVisibility(View.VISIBLE);

                tv_handup.setVisibility(View.VISIBLE);
                tv_break.setVisibility(View.VISIBLE);

                tv_setting.setVisibility(View.GONE);
                tv_background_color.setVisibility(View.GONE);
                tv_foreground_color1.setVisibility(View.GONE);
                tv_foreground_color2.setVisibility(View.GONE);
                tv_background_image_hand.setVisibility(View.GONE);

                tv_clock_interface.setVisibility(View.GONE);

                tv_screen_lock.setVisibility(View.VISIBLE);
                break;
            case SETTING:
                tv_date.setVisibility(View.VISIBLE);
                tv_day.setVisibility(View.VISIBLE);
                tv_weather.setVisibility(View.VISIBLE);
                tv_descript.setVisibility(View.VISIBLE);

                tv_handup.setVisibility(View.VISIBLE);
                tv_break.setVisibility(View.VISIBLE);

                tv_setting.setVisibility(View.VISIBLE);
                tv_background_color.setVisibility(View.VISIBLE);
                tv_foreground_color1.setVisibility(View.VISIBLE);
                tv_foreground_color2.setVisibility(View.VISIBLE);
                tv_background_image_hand.setVisibility(View.VISIBLE);

                tv_clock_interface.setVisibility(View.VISIBLE);

                tv_screen_lock.setVisibility(View.VISIBLE);
                break;
        }
    }

    private boolean isFullScreen(){
        return themeUIViewModel.getClockUITypeEnum().getValue()==ClockThemeUITypeEnum.FULLSCREEN;
    }


    public void screenLock(boolean locked) {
        tv_screen_lock.setImageResource(locked ? R.drawable.ic_screen_lock : R.drawable.ic_screen_unlock);
    }


}
