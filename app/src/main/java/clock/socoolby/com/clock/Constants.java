package clock.socoolby.com.clock;

public  class Constants {
    public static final String APP_NAME="workclock";

    public final static String CLOCK_FOLDER="/CoolClock/";
    public final static int TALKING_HALF_AN_HOUR=1;
    public final static int TALKING_HOURS=2;
    public final static int TALKING_NO_REPORT=3;
    public final static String SHARE_PERFERENCE_FILE="share_perference.conf";
    public final static String TODO_SYNC_PERFERENCE_FILE="todo_perference.conf";
    public final static int SUCCESS_CODE=0;
    public final static int FAIL_CODE=-1;

    public final static String APP_MEMO="小提示: ←动画 →字体 ↑↓亮度 双指_字体大小 双击_切屏 长按_参数设置";

    public static final Integer ACCESSIBILITY_SERVICE_REQUEST_CODE=101;

}