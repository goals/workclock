package clock.socoolby.com.clock.net;


import org.json.JSONObject;
import clock.socoolby.com.clock.ClockApplication;
import clock.socoolby.com.clock.net.listener.AbstractEntityRequestListener;
import clock.socoolby.com.clock.net.protocol.ProtocolConstants;
import clock.socoolby.com.clock.net.protocol.update.UpdateRequest;
import clock.socoolby.com.clock.net.protocol.update.UpdateResponse;
import clock.socoolby.com.clock.net.protocol.weather.WeatherRequest;
import clock.socoolby.com.clock.net.protocol.weather.WeatherResponse;
import clock.socoolby.com.clock.net.listener.RequestListener;
import clock.socoolby.com.clock.utils.FuncUnit;

/**
 * Alway zuo,never die.
 * Created by socoolby on 5/26/16.
 */
public class BusinessService {
    private final static String TAG = BusinessService.class.getSimpleName();

    NetworkManager networkManager;

    public BusinessService(NetworkManager networkManager) {
        this.networkManager = networkManager;
    }

    public void getWeather(String city, RequestListener<WeatherResponse> weatherListListener) {
        if (city == null || city.isEmpty())
            return;
        WeatherRequest request = new WeatherRequest();
        request.setmCity(city);
        networkManager.sendRequest(request, new AbstractEntityRequestListener<WeatherResponse>(weatherListListener){
            @Override
            public WeatherResponse creatEntity() {
                return new WeatherResponse();
            }
        });
    }

    public void checkUpdate() {
        UpdateRequest request = new UpdateRequest();
        networkManager.sendRequest(request, new RequestListener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                UpdateResponse updateResponse = new UpdateResponse(response);
                if (updateResponse.getResultCode() == ProtocolConstants.RESULT_OK) {
                    FuncUnit.openURL(ClockApplication.getContext(), updateResponse.getUpdateURL());
                }
            }

            @Override
            public void onRequestFailed(int error, String errorMessage) {

            }
        });
    }
}
