package clock.socoolby.com.clock.todo.microsoft.listener;

import java.util.List;

import clock.socoolby.com.clock.net.base.I_ResponseState;
import clock.socoolby.com.clock.net.listener.StateAbleRequestListener;
import clock.socoolby.com.clock.todo.microsoft.bean.todo.TodoEntity;

public class TodoEntityListRequestListener extends AbstractMicrosoftEntityListRequestListener<TodoEntity> {
    public TodoEntityListRequestListener(StateAbleRequestListener<List<TodoEntity>, I_ResponseState> warpListener) {
        super(warpListener);
    }

    @Override
    public TodoEntity creatEntity() {
        return new TodoEntity();
    }
}
