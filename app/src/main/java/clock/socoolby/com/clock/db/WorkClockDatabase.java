package clock.socoolby.com.clock.db;

import clock.socoolby.com.clock.Constants;
import e.odbo.data.model.Column;
import e.odbo.data.model.ColumnType;
import e.odbo.data.model.DataBase;
import e.odbo.data.model.Table;
import e.odbo.data.model.TableData;
import e.odbo.data.model.smaple.PK;

public class WorkClockDatabase  extends DataBase {
    public WorkClockDatabase() {
        super(Constants.APP_NAME,"1.0.0");
        initTables();
        initDatas();
    }

    public Table fontStyle,themeUI,alter;

    private void initTables() {
        fontStyle=new Table("time_font_style");
        fontStyle.addColumn(
                Column.c("name", ColumnType.VARCHAR(30),"字体名称").PK(),
                Column.c("display_second", ColumnType.INT,"有秒时的大小").NotNull(),
                Column.c("no_display_second", ColumnType.INT,"无秒时的大小").NotNull(),
                Column.c("display_second_on_full", ColumnType.INT,"全屏有秒时的大小").NotNull(),
                Column.c("no_display_second_on_full", ColumnType.INT,"全屏无秒时的大小").NotNull()
        );
        addTable(fontStyle);

        themeUI=new Table("theme_ui").implement(PK.PK_String);
        themeUI.addColumn(
                Column.c("name",ColumnType.VARCHAR(30),"样式名称").NotNull(),
                Column.c("clock_type", ColumnType.INT,"数字0，模似1").NotNull(),
                Column.c("config_text",ColumnType.VARCHAR(2000),"jsons格式设置").NotNull()
        );
        addTable(themeUI);

         alter=new Table("alter").implement(PK.PK_UUID);
    }

    private void initDatas() {
        TableData fontStyleInitData = TableData.C(fontStyle.getName(),fontStyle.getColumnNames()).types(fontStyle.getColumnDatas());
        fontStyleInitData
                .value("affair", 110, 120, 130, 150)
                .value("agenda", 140, 150, 160, 180)
                .value("cheapfire", 140, 150, 160, 180)
                .value("cherif", 80, 100, 110, 140)
                .value("Cigar Box Guitar", 120, 130, 140, 150)
                .value("Ciung Wanara Sejati", 110, 120, 130, 150)
                .value("DK Bergelmir", 120, 130, 140, 180)
                .value("ds_digi", 140, 150, 160, 200)
                .value("Funk", 110, 120, 130, 150)
                .value("GLADYS Regular", 110, 120, 130, 150)
                .value("Granite Rock St", 110, 120, 130, 170)
                .value("GROOT", 120, 130, 140, 170)
                .value("juleslove", 110, 120, 130, 150)
                .value("Kingthings Annexx", 110, 120, 150, 180)
                .value("Kingthings Willow", 110, 120, 130, 150)
                .value("KYLE Regular", 110, 120, 130, 150)
                .value("LCD-U", 130, 140, 150, 180)
                .value("loong07龙书势如破竹简", 110, 120, 150, 170)
                .value("MILKDROP", 110, 120, 130, 160)
                .value("Mosaicleaf086", 110, 120, 130, 150)
                .value("Pro Display tfb", 130, 140, 150, 170)
                .value("SailingJunco", 130, 140, 150, 200)
                .value("scoreboard", 120, 130, 140, 190)
                .value("SFWasabi-Bold", 110, 120, 130, 140)
                .value("Spaghettica", 110, 120, 130, 150)
                .value("the_vandor_spot", 140, 150, 160, 210)
                .value("Woodplank", 120, 130, 140, 180)
                .value("Xtra Power", 100, 110, 120, 160)
                .value("海报圆圆", 110, 120, 130, 150)
                .value("爱心小兔", 110, 115, 130, 140)
                .value("王漢宗海報體一半天水", 110, 120, 130, 150)
                .value("立体铁山硬笔行楷简", 120, 130, 140, 180)
                .value("腾祥倩心简", 110, 120, 130, 150)
                .value("苏新诗毛糙体简", 110, 120, 130, 150)
                .value("谁能许我扶桑花期", 110, 120, 130, 150)
                .value("造字工房凌黑", 110, 120, 130, 150)
                .value("中国龙新草体", 110, 120, 130, 150)
                .value("迷你简剪纸_0",110,120,130,150)
                .value("BONX-TubeBold",110,120,130,150)
                .value("BONX-TubeBoldReverse",110,120,130,150)
        ;
        addData(fontStyleInitData);
    }
}
