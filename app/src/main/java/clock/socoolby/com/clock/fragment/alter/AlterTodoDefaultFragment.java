package clock.socoolby.com.clock.fragment.alter;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.idanatz.oneadapter.OneAdapter;
import com.idanatz.oneadapter.external.modules.EmptinessModule;
import com.idanatz.oneadapter.external.modules.EmptinessModuleConfig;
import com.iigo.library.ClockHelper;
import com.iigo.library.ClockView;

import java.util.Calendar;
import java.util.List;

import clock.socoolby.com.clock.R;
import clock.socoolby.com.clock.event.ClockEvent;
import clock.socoolby.com.clock.event.ClockEventListener;
import clock.socoolby.com.clock.event.EventManger;
import clock.socoolby.com.clock.state.ClockStateMachine;
import clock.socoolby.com.clock.todo.microsoft.adapter.TodoEntityAdapter;
import clock.socoolby.com.clock.todo.microsoft.bean.todo.TodoEntity;

public class AlterTodoDefaultFragment extends AbstractAlterFragment {
    List<TodoEntity> todayTodoList;

    RelativeLayout clockViewContainer;
    ClockView clockView;

    RecyclerView todayTodoListView;

    Button button_5,button_10,button_15,button_30;

    ClockHelper clockHelper;

    Calendar  calendar;

    TextView tv_todo_name,tv_todo_memo;

    TodoEntity currentTodoEntity;
    private OneAdapter todoAdapter;

    public AlterTodoDefaultFragment() {
        super(R.layout.fragment_alter_todo);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        todayTodoList=globalViewModel.getTodoSyncServiceManager().getTodayTodoEntities();
        currentTodoEntity=clockStateMachine.getCurrentTodoEntity();
    }

    @Override
    void bindView(View rootView) {
        clockViewContainer =rootView.findViewById(R.id.clockView_container);
        clockViewContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endAlter(null);
            }
        });
        clockView=rootView.findViewById(R.id.todo_clockView);
        todayTodoListView=rootView.findViewById(R.id.today_todo_list_view);
        button_5=rootView.findViewById(R.id.button_5_minute);
        button_5.setOnClickListener(v->{
            calendar.add(Calendar.MINUTE,5);
            endAlter(calendar.getTime());
        });
        button_10=rootView.findViewById(R.id.button_10_minute);
        button_5.setOnClickListener(v->{
            calendar.add(Calendar.MINUTE,10);
            endAlter(calendar.getTime());
        });
        button_15=rootView.findViewById(R.id.button_15_minute);
        button_5.setOnClickListener(v->{
            calendar.add(Calendar.MINUTE,15);
            endAlter(calendar.getTime());

        });
        button_30=rootView.findViewById(R.id.button_30_minute);
        button_5.setOnClickListener(v->{
            calendar.add(Calendar.MINUTE,30);
            endAlter(calendar.getTime());
        });
        clockHelper = new ClockHelper(clockView); //创建 'ClockHelper' 对象.

        tv_todo_name=rootView.findViewById(R.id.todo_tv_name);
        tv_todo_memo=rootView.findViewById(R.id.todo_tv_memo);

        todayTodoListView.setLayoutManager(new LinearLayoutManager(rootView.getContext()));

        todoAdapter = new OneAdapter()
                .attachItemModule(new TodoEntityAdapter())
                .attachEmptinessModule(new EmptinessModule() {
                    @Override
                    public EmptinessModuleConfig provideModuleConfig() {
                        return new EmptinessModuleConfig() {
                            @Override
                            public int withLayoutResource() {
                                return R.layout.entity_null;
                            }
                        };
                    }
                })
                .attachTo(todayTodoListView);
    }

    @Override
    public void onResume() {
        super.onResume();
        clockHelper.start();
        clockHelper.goOff();
    }

    @Override
    public void onPause() {
        super.onPause();
        clockHelper.stop();
    }

    @Override
    void bindViewModel() {
        calendar=Calendar.getInstance();
        EventManger.addHeartbeatListener(this, new ClockEventListener<Boolean>() {
            @Override
            public void onEvent(ClockEvent<Boolean> event) {
                playHandUpMusic();
            }
        });
        if(currentTodoEntity!=null){
            tv_todo_name.setText(currentTodoEntity.getSubject());
            tv_todo_memo.setText(currentTodoEntity.getBody().getContent());
        }
        todoAdapter.setItems(todayTodoList);
    }
}
