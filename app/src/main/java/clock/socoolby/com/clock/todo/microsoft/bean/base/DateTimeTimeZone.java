package clock.socoolby.com.clock.todo.microsoft.bean.base;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.Date;

import clock.socoolby.com.clock.net.base.I_JsonObject;
import clock.socoolby.com.clock.todo.microsoft.utils.TypeUtils;

public class DateTimeTimeZone implements I_JsonObject {
    Date dateTime;
    String timeZone;

    @Override
    public void fromJson(JSONObject jsonObject) throws JSONException {
        timeZone=jsonObject.getString("timeZone");
        try {
            dateTime= TypeUtils.deserialize(jsonObject.getString("dateTime"),timeZone);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void toJson(JSONObject jsonObject) throws JSONException {

    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }
}
