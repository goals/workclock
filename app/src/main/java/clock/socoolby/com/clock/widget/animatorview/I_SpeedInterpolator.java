package clock.socoolby.com.clock.widget.animatorview;

//数值发生器
public interface I_SpeedInterpolator<T extends Number> {
    //加速百分比0-100
    void speedUp(int percent);

    void speedDown(int percent);

    void resetSpeed();

    void start();

    void stop();

    T move();

}
