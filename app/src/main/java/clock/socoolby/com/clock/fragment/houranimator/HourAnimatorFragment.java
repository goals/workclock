package clock.socoolby.com.clock.fragment.houranimator;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import clock.socoolby.com.clock.R;
import clock.socoolby.com.clock.event.ClockEvent;
import clock.socoolby.com.clock.event.ClockEventListener;
import clock.socoolby.com.clock.event.EventManger;
import clock.socoolby.com.clock.fragment.AbstractAnimatorFragment;
import clock.socoolby.com.clock.widget.animatorview.I_Animator;

/**
 * A simple {@link Fragment} subclass.
 */
public class HourAnimatorFragment extends AbstractAnimatorFragment {

    private int animatorDialy;

    public HourAnimatorFragment(I_Animator hourAnimator) {
        super(hourAnimator);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_hour_animator, container, false);
        initAnimatorView(view.findViewById(R.id.tv_hour_animatorview));
        bindViewModel();
        //Log.d("animator fragment","on create animator view width:"+animatorView.getWidth());
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
    }

    protected void bindViewModel(){
        animatorDialy=globalViewModel.getTimeHourAnimatordialy().getValue();
        globalViewModel.getForegroundColor().observe(getActivity(), new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                animatorView.setColor(integer);
            }
        });

        EventManger.addHeartbeatListener(this, new ClockEventListener<Boolean>() {
            @Override
            public void onEvent(ClockEvent<Boolean> event) {
                Log.d("hour Animator", "go in heartBeat Observer:"+animatorDialy);
                if(animatorDialy==0) {
                    Log.d("hour Animator", "animator runDelay is out ,end animator");
                    animatorDialy=globalViewModel.getTimeHourAnimatordialy().getValue();
                    EventManger.post(ClockEvent.buildHourAnimator(false));
                }
                animatorDialy--;
            }
        });
    }
}
