package clock.socoolby.com.clock;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;

import timber.log.Timber;

public class ActivateAdmin extends DeviceAdminReceiver {

    @Override
    public void onDisabled(Context context, Intent intent) {
        super.onDisabled(context, intent);
        Timber.d("Disabled lock device admin");
    }
}
