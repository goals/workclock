package clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.pointer;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.I_Pointer;

public abstract class AbstractPointer implements I_Pointer {

    protected Paint  mPointerPaint;

    protected float mRadius, mPointRadius,
            mHourPointerLength, mHourPointerWidth,
            mMinutePointerLength, mMinutePointerWidth,
            mSecondPointerLength, mSecondPointerWidth;


    protected int  mClockColor, mColorHourPointer= Color.BLACK,mColorMinutePointer= Color.BLACK, mColorSecondPointer= Color.BLACK;

    // 当前时、分、秒
    protected int mH, mM, mS;

    @Override
    public void init(float mRadius,int mClockColor) {
        this.mRadius = mRadius;
        this.mClockColor = mClockColor;
        initPaint();
        initPointerLength(mRadius);
    }


    public void initPaint(){
        mPointerPaint = new Paint();
        mPointerPaint.setAntiAlias(true);
        mPointerPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mPointerPaint.setStrokeCap(Paint.Cap.ROUND);
    }

    public void setmPointerColor(int color){
        mColorHourPointer=mColorMinutePointer=color;
    }

    @Override
    public void setColorSecond(int colorSecond) {
        mColorSecondPointer=colorSecond;
    }

    public  void initPointerLength(float mRadius){
        /*
         * 默认时钟刻度长=半径/10;
         * 默认时钟刻度宽=长/6;
         *
         * */
        float mDefaultScaleLength = mRadius / 10;
        float mDefaultScaleWidth = mDefaultScaleLength / 6;

        /*
         * 特殊时钟刻度长=半径/5;
         * 特殊时钟刻度宽=长/6;
         *
         * */
        float mParticularlyScaleLength = mRadius / 5;
        float mParticularlyScaleWidth = mParticularlyScaleLength / 6;

        /*
         * 时针长=半径/3;
         * 时针宽=特殊时钟刻度宽;
         *
         * */
        mHourPointerLength = mRadius / 3;
        mHourPointerWidth = mParticularlyScaleWidth;

        /*
         * 分针长=半径/2;
         * 分针宽=特殊时钟刻度宽;
         *
         * */
        mMinutePointerLength = mRadius / 2;
        mMinutePointerWidth = mParticularlyScaleWidth;

        /*
         * 秒针长=半径/3*2;
         * 秒针宽=默认时钟刻度宽;
         *
         * */
        mSecondPointerLength = mRadius / 3 * 2;
        mSecondPointerWidth = mDefaultScaleWidth;

        // 中心点半径=（默认刻度宽+特殊刻度宽）/2
        mPointRadius = (mDefaultScaleWidth + mParticularlyScaleWidth) / 2;
    }

    /**
     * 绘制指针
     */
    public void drawPointer(Canvas canvas, int mH,int mM,int mS, Paint paint) {
        this.mH=mH;
        this.mM=mM;
        this.mS=mS;

        drawHourPointer(canvas);
        drawMinutePointer(canvas);
        drawSecondPointer(canvas);

        drawCenterCircle(canvas);
    }

    // 绘制中心原点，需要在指针绘制完成后才能绘制
    protected void drawCenterCircle(Canvas canvas){
        canvas.drawCircle(0, 0, mPointRadius, mPointerPaint);
    }

    /**
     * 绘制时针
     */
    protected void drawHourPointer(Canvas canvas) {

        mPointerPaint.setStrokeWidth(mHourPointerWidth);
        mPointerPaint.setColor(mColorHourPointer);

        // 当前时间的总秒数
        float s = mH * 60 * 60 + mM * 60 + mS;
        // 百分比
        float percentage = s / (12 * 60 * 60);
        // 通过角度计算弧度值，因为时钟的角度起线是y轴负方向，而View角度的起线是x轴正方向，所以要加270度
        float angle = calcAngle(percentage);

        float x = calcX(mHourPointerLength , angle);
        float y = calcY(mHourPointerLength , angle);

        canvas.drawLine(0, 0, x, y, mPointerPaint);
    }

    /**
     * 绘制分针
     */
    protected void drawMinutePointer(Canvas canvas) {
        mPointerPaint.setStrokeWidth(mMinutePointerWidth);
        mPointerPaint.setColor(mColorMinutePointer);

        float s = mM * 60 + mS;
        float percentage = s / (60 * 60);
        float angle = calcAngle(percentage);

        float x = calcX(mMinutePointerLength , angle);
        float y = calcY(mMinutePointerLength , angle);

        canvas.drawLine(0, 0, x, y, mPointerPaint);
    }

    /**
     * 绘制秒针
     */
    protected void drawSecondPointer(Canvas canvas) {

        mPointerPaint.setStrokeWidth(mSecondPointerWidth);
        mPointerPaint.setColor(mColorSecondPointer);

        float s = mS;
        float percentage = s / 60;
        float angle = calcAngle(percentage);

        float x = calcX(mSecondPointerLength , angle);
        float y = calcY(mSecondPointerLength , angle);

        canvas.drawLine(0, 0, x, y, mPointerPaint);
    }

    protected float calcAngle(float percentage){
        return 270 + 360 * percentage;
    }

    protected float calcX(float baseLength,float angle){
        return (float)(baseLength * Math.cos(Math.PI * 2 / 360 * angle));
    }

    protected float calcY(float baseLength,float angle){
        return (float) (baseLength * Math.sin(Math.PI * 2 / 360 * angle));
    }

}
