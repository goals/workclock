package clock.socoolby.com.clock.todo.microsoft.listener;

import java.util.List;

import clock.socoolby.com.clock.net.base.I_ResponseState;
import clock.socoolby.com.clock.net.listener.StateAbleRequestListener;
import clock.socoolby.com.clock.todo.microsoft.bean.todo.TodoGroup;

public class TodoGroupListRequestListener extends AbstractMicrosoftEntityListRequestListener<TodoGroup> {

    public TodoGroupListRequestListener(StateAbleRequestListener<List<TodoGroup>, I_ResponseState> warpListener) {
        super(warpListener);
    }

    @Override
    public TodoGroup creatEntity() {
        return new TodoGroup();
    }
}
