package clock.socoolby.com.clock.widget.animatorview;

import android.content.Context;
import android.graphics.Canvas;
import android.view.View;

public interface I_Animator {

    void init(Context context, View main);

    void onDraw(Canvas canvas);

    void start();

    void pause();

    void stop();

    boolean isRunning();

    void onSizeChanged(int w, int h, int oldw, int oldh);

    void setColor(int color);

    void setRandColor(boolean randColor);

    void restart();

    boolean isColorUseAble();

    boolean isRandColor();
}
