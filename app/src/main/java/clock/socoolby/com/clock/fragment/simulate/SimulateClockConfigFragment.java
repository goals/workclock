package clock.socoolby.com.clock.fragment.simulate;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.openbravo.data.basic.BasicException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import clock.socoolby.com.clock.ClockApplication;
import clock.socoolby.com.clock.R;
import clock.socoolby.com.clock.ThemeUIManager;
import clock.socoolby.com.clock.pop.ColorPickerPop;
import clock.socoolby.com.clock.state.ClockInterfaceTypeEnum;
import clock.socoolby.com.clock.utils.DialogUtils;
import clock.socoolby.com.clock.viewmodel.GlobalViewModel;
import clock.socoolby.com.clock.viewmodel.SimulateViewModel;
import clock.socoolby.com.clock.viewmodel.ThemeUIViewModel;
import clock.socoolby.com.clock.viewmodel.ViewModelFactory;
import clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.pointer.DefaultPointer;
import clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.pointer.LeafPointer;
import clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.pointer.LeafTwoPointer;
import clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.pointer.SecondTailPointer;
import clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.pointer.SwordPointer;
import clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.pointer.TrianglePointer;
import clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.pointer.TwoStepPointer;

public class SimulateClockConfigFragment extends Fragment {

    public static final String TAG = SimulateClockFragment.class.getSimpleName();

    public static final String NAME = "simulateClockConfig";

    ThemeUIManager themeUIManager;


    SimulateViewModel simulateViewModel;
    GlobalViewModel globalViewModel;
    ThemeUIViewModel themeUIViewModel;

    @BindView(R.id.tv_simulate_color_pointer)
    Button tvSimulateColorPointer;
    @BindView(R.id.tv_simulate_color_scale)
    Button tvSimulateColorScale;
    @BindView(R.id.tv_simulate_color_scalep_particularly)
    Button tvSimulateColorScalepParticularly;
    @BindView(R.id.tv_simulate_color_text)
    Button tvSimulateColorText;
    @BindView(R.id.tv_simulate_color_outline)
    Button tvSimulateColorOutline;
    @BindView(R.id.textView4)
    TextView textView4;
    @BindView(R.id.tv_simulate_text_show_hide)
    RadioButton tvSimulateTextShowHide;
    @BindView(R.id.tv_simulate_text_show_all)
    RadioButton tvSimulateTextShowAll;
    @BindView(R.id.tv_simulate_text_show_four)
    RadioButton tvSimulateTextShowFour;
    @BindView(R.id.tv_simulate_text_show_group)
    RadioGroup tvSimulateTextShowGroup;
    @BindView(R.id.textView5)
    TextView textView5;
    @BindView(R.id.tv_simulate_pointer_style_1)
    Button tvSimulatePointerStyle1;
    @BindView(R.id.tv_simulate_pointer_style_2)
    Button tvSimulatePointerStyle2;
    @BindView(R.id.tv_simulate_pointer_style_3)
    Button tvSimulatePointerStyle3;
    @BindView(R.id.tv_simulate_pointer_style_4)
    Button tvSimulatePointerStyle4;
    @BindView(R.id.pointer_list)
    ScrollView pointerList;
    @BindView(R.id.tv_simulate_pointer_style_0)
    Button tvSimulatePointerStyle0;
    @BindView(R.id.tv_simulate_pointer_style_5)
    Button tvSimulatePointerStyle5;
    @BindView(R.id.tv_simulate_pointer_style_6)
    Button tvSimulatePointerStyle6;
    @BindView(R.id.tv_simulate_color_pointer_second)
    Button tvSimulateColorPointerSecond;
    @BindView(R.id.tv_themeUI_style_1)
    Button tvThemeUIStyle1;
    @BindView(R.id.tv_themeUI_style_2)
    Button tvThemeUIStyle2;
    @BindView(R.id.tv_themeUI_style_3)
    Button tvThemeUIStyle3;
    @BindView(R.id.tv_themeUI_style_4)
    Button tvThemeUIStyle4;
    @BindView(R.id.tv_theme_config_recover)
    Button tvThemeConfigRecover;

    private Unbinder unbinder;

    private ColorPickerPop colorPickerDialog;

    public SimulateClockConfigFragment(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        globalViewModel = ViewModelProviders.of(getActivity(), new ViewModelFactory(ClockApplication.getInstance().getModel())).get(GlobalViewModel.class);
        simulateViewModel = ViewModelProviders.of(getActivity(), new ViewModelFactory(ClockApplication.getInstance().getModel())).get(SimulateViewModel.class);
        themeUIViewModel=ViewModelProviders.of(getActivity(), new ViewModelFactory(ClockApplication.getInstance().getModel())).get(ThemeUIViewModel.class);
        themeUIManager=globalViewModel.getThemeUIManager();
        themeUIManager.saveTempThemeUI(ClockInterfaceTypeEnum.Simulate.code);
        globalViewModel.setAppConfig(true);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_simulate_config, container, false);
        unbinder = ButterKnife.bind(this, view);

        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //getFragmentManager().popBackStack();
                themeUIViewModel.loadFromModel();
                return true;
            }
        });

        bindViewModel();

        tvSimulateTextShowGroup.setOnCheckedChangeListener((view1, checkId) -> {
            switch (checkId) {
                case R.id.tv_simulate_text_show_hide:
                    break;
                case R.id.tv_simulate_text_show_all:
                    break;
                case R.id.tv_simulate_text_show_four:
                    break;
            }
        });

        tvSimulatePointerStyle0.setOnClickListener((e) -> simulateViewModel.setPointerTypeName(DefaultPointer.TYPE_NAME));
        tvSimulatePointerStyle1.setOnClickListener((e) -> simulateViewModel.setPointerTypeName(LeafPointer.TYPE_LEAF));
        tvSimulatePointerStyle2.setOnClickListener((e) -> simulateViewModel.setPointerTypeName(LeafTwoPointer.TYPE_LEAF_TWO));
        tvSimulatePointerStyle3.setOnClickListener((e) -> simulateViewModel.setPointerTypeName(SecondTailPointer.TYPE_SECOND_TAIL));
        tvSimulatePointerStyle4.setOnClickListener((e) -> simulateViewModel.setPointerTypeName(SwordPointer.TYPE_SWORD_POINTER));
        tvSimulatePointerStyle5.setOnClickListener((e) -> simulateViewModel.setPointerTypeName(TrianglePointer.TYPE_TRIANGLE_POINTER));
        tvSimulatePointerStyle6.setOnClickListener((e) -> simulateViewModel.setPointerTypeName(TwoStepPointer.TYPE_TWO_STEP_POINTER));


        tvSimulateColorPointer.setOnClickListener((view1) -> {
            colorSelect(simulateViewModel.getSimulateClockColorPointer().getValue(),
                    new ColorPickerPop.OnColorListener() {
                        @Override
                        public void onEnsure(int color) {
                            simulateViewModel.setSimulateClockColorPointer(color);
                            view1.setBackgroundColor(color);
                        }

                        @Override
                        public void onBack() {
                        }
                    });
        });

        tvSimulateColorPointerSecond.setOnClickListener((view1) -> {
            colorSelect(simulateViewModel.getSimulateClockColorPointerSecond().getValue(),
                    new ColorPickerPop.OnColorListener() {
                        @Override
                        public void onEnsure(int color) {
                            simulateViewModel.setSimulateClockColorPointerSecond(color);
                            view1.setBackgroundColor(color);
                        }

                        @Override
                        public void onBack() {
                        }
                    });
        });

        tvSimulateColorScale.setOnClickListener((view1) -> {
            colorSelect(simulateViewModel.getSimulateClockColorScale().getValue(),
                    new ColorPickerPop.OnColorListener() {
                        @Override
                        public void onEnsure(int color) {
                            simulateViewModel.setSimulateClockColorScale(color);
                            view1.setBackgroundColor(color);
                        }

                        @Override
                        public void onBack() {
                        }
                    });
        });

        tvSimulateColorScalepParticularly.setOnClickListener((view1) -> {
            colorSelect(simulateViewModel.getSimulateClockColorScaleParticularly().getValue(),
                    new ColorPickerPop.OnColorListener() {
                        @Override
                        public void onEnsure(int color) {
                            simulateViewModel.setSimulateClockColorScaleParticularly(color);
                            view1.setBackgroundColor(color);
                        }

                        @Override
                        public void onBack() {
                        }
                    });
        });

        tvSimulateColorOutline.setOnClickListener((view1) -> {
            colorSelect(simulateViewModel.getSimulateClockColorOutLine().getValue(),
                    new ColorPickerPop.OnColorListener() {
                        @Override
                        public void onEnsure(int color) {
                            simulateViewModel.setSimulateClockColorOutLine(color);
                            view1.setBackgroundColor(color);
                        }

                        @Override
                        public void onBack() {
                        }
                    });
        });

        tvSimulateColorText.setOnClickListener((view1) -> {
            colorSelect(simulateViewModel.getSimulateClockColorText().getValue(),
                    new ColorPickerPop.OnColorListener() {
                        @Override
                        public void onEnsure(int color) {
                            simulateViewModel.setSimulateClockColorText(color);
                            view1.setBackgroundColor(color);
                        }

                        @Override
                        public void onBack() {
                        }
                    });
        });

        tvThemeUIStyle1.setOnClickListener(v -> changeThemeUIStyle(1));

        tvThemeUIStyle2.setOnClickListener(v -> changeThemeUIStyle(2));

        tvThemeUIStyle3.setOnClickListener(v -> changeThemeUIStyle(3));

        tvThemeUIStyle4.setOnClickListener(v -> changeThemeUIStyle(4));

        tvThemeUIStyle1.setOnLongClickListener(v -> configThemeUIStyle(1));

        tvThemeUIStyle2.setOnLongClickListener(v -> configThemeUIStyle(2));

        tvThemeUIStyle3.setOnLongClickListener(v -> configThemeUIStyle(3));

        tvThemeUIStyle4.setOnLongClickListener(v -> configThemeUIStyle(4));

        tvThemeConfigRecover.setOnClickListener(v-> {
            try {
                themeUIManager.recoverTempThemeUI(ClockInterfaceTypeEnum.Simulate.code);
                reloadViewModel();
            } catch (BasicException e) {
                e.printStackTrace();
            }
        });

        return view;
    }

    private void changeThemeUIStyle(int order) {
        String styleName = "simulate_style_" + order;
        try {
            if (themeUIManager.exitsThemeUIStyle(ClockInterfaceTypeEnum.Simulate.code, styleName)) {
                themeUIManager.loadSimulateThemeFromDB(styleName);
                reloadViewModel();
            } else {
                DialogUtils.show(getActivity(),"温馨提示","当前主题还未设置,是否以当前主题保存.", ok->{
                    if(ok)
                        configThemeUIStyle(order);
                });
                Toast.makeText(getActivity(), "你可长按来保存一个主题", Toast.LENGTH_SHORT).show();
            }
        } catch (BasicException e) {
            e.printStackTrace();
        }
    }

    private boolean configThemeUIStyle(int order) {
        String styleName = "simulate_style_" + order;
        try {
            themeUIManager.saveSimulateThemeFromModel(styleName);
            Toast.makeText(getActivity(), "当前主题已保存", Toast.LENGTH_SHORT).show();
        } catch (BasicException e) {
            e.printStackTrace();
        }
        return true;
    }


    private void colorSelect(int defColor, ColorPickerPop.OnColorListener listener) {
        if (colorPickerDialog == null)
            colorPickerDialog = new ColorPickerPop(getActivity());
        colorPickerDialog.setOnColorChangeListenter(listener);
        colorPickerDialog.show(defColor);
    }

    private void reloadViewModel(){
        globalViewModel.loadFromModel();
        //simulateViewModel.loadFromModel();
        //loadForViewModel();
    }

    private void bindViewModel() {
        globalViewModel.getForegroundColor().observe(this, integer -> setTextColor(integer));
    }

    /**
     * onDestroyView中进行解绑操作
     */
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        globalViewModel.setAppConfig(false);
        unbinder.unbind();
    }

    private void setTextColor(int color) {

        tvSimulateColorPointer.setBackgroundColor(simulateViewModel.getSimulateClockColorPointer().getValue());
        tvSimulateColorPointerSecond.setBackgroundColor(simulateViewModel.getSimulateClockColorPointerSecond().getValue());
        tvSimulateColorScale.setBackgroundColor(simulateViewModel.getSimulateClockColorScale().getValue());
        tvSimulateColorScalepParticularly.setBackgroundColor(simulateViewModel.getSimulateClockColorScaleParticularly().getValue());
        tvSimulateColorOutline.setBackgroundColor(simulateViewModel.getSimulateClockColorOutLine().getValue());
        tvSimulateColorText.setBackgroundColor(simulateViewModel.getSimulateClockColorText().getValue());

        tvSimulateColorPointer.setTextColor(color);
        tvSimulateColorPointerSecond.setTextColor(color);
        tvSimulateColorScale.setTextColor(color);
        tvSimulateColorScalepParticularly.setTextColor(color);
        tvSimulateColorOutline.setTextColor(color);
        tvSimulateColorText.setTextColor(color);

        tvThemeUIStyle1.setTextColor(color);
        tvThemeUIStyle2.setTextColor(color);
        tvThemeUIStyle3.setTextColor(color);
        tvThemeUIStyle4.setTextColor(color);

        tvSimulatePointerStyle0.setTextColor(color);
        tvSimulatePointerStyle1.setTextColor(color);
        tvSimulatePointerStyle2.setTextColor(color);
        tvSimulatePointerStyle3.setTextColor(color);
        tvSimulatePointerStyle4.setTextColor(color);
        tvSimulatePointerStyle5.setTextColor(color);
        tvSimulatePointerStyle6.setTextColor(color);

        textView4.setTextColor(color);
        textView5.setTextColor(color);

        tvSimulateTextShowHide.setTextColor(color);
        tvSimulateTextShowAll.setTextColor(color);
        tvSimulateTextShowFour.setTextColor(color);
    }
}
