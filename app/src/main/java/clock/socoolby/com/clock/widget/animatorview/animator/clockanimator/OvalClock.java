package clock.socoolby.com.clock.widget.animatorview.animator.clockanimator;

import android.graphics.Canvas;
import android.graphics.Paint;

//(假）椭圆
public class OvalClock extends AbstractClock {

    /**
     * 绘制时钟的圆形和刻度
     */
    protected void drawBorder(Canvas canvas) {
        canvas.save();
        canvas.translate(mCenterX, mCenterY);
        mDefaultPaint.setStrokeWidth(mDefaultScaleWidth);
        mDefaultPaint.setColor(mClockColor);
        canvas.scale((width/2)/(mRadius), 1);
        canvas.drawCircle(0, 0, mRadius, mDefaultPaint);

        for (int i = 0; i < 60; i++) {
            if (i % 5 == 0) { // 特殊时刻

                mDefaultPaint.setStrokeWidth(mParticularlyScaleWidth);
                mDefaultPaint.setColor(mColorParticularyScale);
                mDefaultPaint.setAlpha(255);
                canvas.drawLine(0, -mRadius, 0, -mRadius + mParticularlyScaleLength, mDefaultPaint);

            } else {          // 一般时刻

                mDefaultPaint.setStrokeWidth(mDefaultScaleWidth);
                mDefaultPaint.setColor(mColorDefaultScale);
                mDefaultPaint.setAlpha(100);
                canvas.drawLine(0, -mRadius, 0, -mRadius + mDefaultScaleLength, mDefaultPaint);

            }
            canvas.rotate(6);
        }

        canvas.restore();
    }

    @Override
    public String typeName() {
        return TYPE_OVAL;
    }

    public static final String TYPE_OVAL="oval";

}