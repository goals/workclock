package clock.socoolby.com.clock.widget.animatorview.animator;


import android.graphics.Canvas;
import android.graphics.Paint;

import clock.socoolby.com.clock.widget.animatorview.AbstractAnimator;
import clock.socoolby.com.clock.widget.animatorview.I_AnimatorEntry;

//引用自:https://blog.csdn.net/u010386612/article/details/50580080
public class BubbleCollisionAnimator extends AbstractAnimator<BubbleCollisionAnimator.Ball> {

    public static final String NAME="BubbleCollision";

    private int maxRadius;  // 小球最大半径
    private int minRadius; // 小球最小半径
    private int minSpeed = 5; // 小球最小移动速度
    private int maxSpeed = 20; // 小球最大移动速度

    public BubbleCollisionAnimator(int entryQuantity) {
        super(entryQuantity);
    }

    public BubbleCollisionAnimator() {
        super(20);
    }


    @Override
    public boolean run() {
        // 球碰撞边界
        for (Ball ball:list) {
            collisionDetectingAndChangeSpeed(ball); // 碰撞边界的计算
            ball.move(width,height); // 移动
        }
        return true;
    }

    @Override
    protected void initPaint(Paint mPaint) {
        // 设置画笔
        mPaint= new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setAlpha(180);
        mPaint.setStrokeWidth(0);
    }

    @Override
    public Ball createNewEntry() {
        maxRadius = width/12;
        minRadius = maxRadius/2;
        Ball mBall=new Ball();
        randomColorIfAble();
        // 设置速度
        float speedX = (rand.nextInt(maxSpeed -minSpeed +1)+5)/10f;
        float speedY = (rand.nextInt(maxSpeed -minSpeed +1)+5)/10f;
        mBall.color = color;
        mBall.vx = rand.nextBoolean() ? speedX : -speedX;
        mBall.vy = rand.nextBoolean() ? speedY : -speedY;
        mBall.radius = rand.nextInt(maxRadius+1 - minRadius) +minRadius;
        mBall.cx=mBall.cy=0;
        if(rand.nextBoolean())
            mBall.cx=rand.nextInt(width);
        else
            mBall.cy=rand.nextInt(height);
        return mBall;
    }


    // 判断球是否碰撞碰撞边界
    public void collisionDetectingAndChangeSpeed(Ball ball) {
        int left = 0;
        int top = 0;
        int right = width;
        int bottom = height;

        float speedX = ball.vx;
        float speedY = ball.vy;

        // 碰撞左右，X的速度取反。 speed的判断是防止重复检测碰撞，然后黏在墙上了=。=
        if(ball.left() <= left && speedX < 0) {
            ball.vx = -ball.vx;
        } else if(ball.top() <= top && speedY < 0) {
            ball.vy = -ball.vy;
        } else if(ball.right() >= right && speedX >0) {
            ball.vx = -ball.vx;
        } else if(ball.bottom() >= bottom && speedY >0) {
            ball.vy = -ball.vy;
        }
    }


    public class Ball implements I_AnimatorEntry {
        int radius; // 半径
        float cx;   // 圆心
        float cy;   // 圆心
        float vx; // X轴速度
        float vy; // Y轴速度
        int color;//颜色

        // 移动
        public void move(int maxWidth, int maxHight) {
            //向角度的方向移动，偏移圆心
            cx += vx;
            cy += vy;
        }

        int left() {
            return (int) (cx - radius);
        }

        int right() {
            return (int) (cx +radius);
        }

        int bottom() {
            return (int) (cy + radius);
        }

        int top() {
            return (int) (cy - radius);
        }



        @Override
        public void onDraw(Canvas canvas, Paint mPaint) {
            mPaint.setColor(color);
            canvas.drawCircle(cx, cy, radius, mPaint);
        }

        @Override
        public void setAnimatorEntryColor(int color) {
             this.color=color;
        }


    }
}
