package clock.socoolby.com.clock.viewmodel;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.annotation.NonNull;

import clock.socoolby.com.clock.FontManager;
import clock.socoolby.com.clock.model.SharePerferenceModel;
import e.odbo.data.dao.EntityManager;

public class ViewModelFactory extends ViewModelProvider.NewInstanceFactory{

    SharePerferenceModel model;


    public ViewModelFactory(SharePerferenceModel model) {
        this.model = model;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if(GlobalViewModel.class.isAssignableFrom(modelClass))
            return (T)new GlobalViewModel(model);
        if(SimulateViewModel.class.isAssignableFrom(modelClass))
            return (T)new SimulateViewModel(model);
        if(DigitViewModel.class.isAssignableFrom(modelClass))
            return (T)new DigitViewModel(model);
        if(ThemeUIViewModel.class.isAssignableFrom(modelClass))
            return (T)new ThemeUIViewModel(model);
        if(AlterViewModel.class.isAssignableFrom(modelClass))
            return (T)new AlterViewModel(model);
        return super.create(modelClass);
    }
}
